# Online ATM v2

This project is for cra-template with Typscript template.

## How to run this app.

- First you will need to clone the node app.
- Then you can use **yarn** or **npm** to install the necessary packages.
- Then you can use **yarn start** or **npm run start** to run the application.
